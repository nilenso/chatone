defmodule Chatone do
  use Application

  @moduledoc """
  Documentation for Chatone.
  """

  @doc """
  """
  def start(_type, _args) do
    IO.inspect("Hello", label: "Hi")

    children = [
      Registry.child_spec(keys: :unique, name: Chatone.OnlineUsers)
    ]

    Supervisor.start_link(children, strategy: :one_for_one)
  end
end

"""
Connected user - User
"""
