defmodule Chatone.User do
  use GenServer

  def user(user_id) do
    "user-#{user_id}"
  end

  def start_link(registry_name, user_id) do
    GenServer.start_link(
      __MODULE__,
      {registry_name, user_id},
      name: {:via, Registry, {registry_name, user(user_id)}}
    )
  end

  def init({registry_name, user_id}) do
    # initialize websocket connection, put that in the state
    {:ok, {registry_name, user_id}}
  end

  def send_message(from, to, message) do
    [{to_pid, _val}] = Registry.lookup(Chatone.OnlineUsers, user(to))
    GenServer.call(to_pid, {:message, from, to, message})
  end

  def handle_call({:message, from, to, message}, _from, state) do
    # send received message over WS in state
    IO.puts("(#{to}) #{from}: #{message}")
    {:reply, :ok, state}
  end
end
